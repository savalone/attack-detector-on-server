## Attack-detector-on-server 

the first thing is ... if your server is not directly  in the internet ... you will probably will have no DOS attacks via ssh
and fail2ban-client will report no attacks
but event that way .... try to access this server you are working on ... using wrong login/username
several times and then, you will be blocked

To test it, you just need to try logging into the promo server using wrong credentials, for 3 times. That will block you for 24 hours.
The fail2ban in clearos uses the ipset command, which is part of IPtables.
List banned ips:

```
- fail2ban-client status sshd

- ipset list 

- systemctl restart fail2ban 

- ipset list f2b-sshd

```

But the thing is not working is the timeout. Even having that set 86400 seconds (24 hours) in /etc/fail2ban/jail.d/clearos-sshd.conf, 
it will block IPs for just 600 seconds (10 minutes). What I did for tiki.suite.wiki was changing the default time taken 
from fail2ban action, by changing the file /etc/fail2ban/action.d/iptables-ipset-proto6-allports.conf.
Look for bantime in that file, change it to 86400 (24 hours) and restart fail2ban.
